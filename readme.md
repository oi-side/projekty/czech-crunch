Webová arkádovka pro [CzechCrunch](https://cc.cz) herní speciál! 🚙 Hru si můžeš zahrát ve svém prohlížeči na [GitLab Pages](https://oi-side.pages.fel.cvut.cz/projekty/czech-crunch/). 🎮

Tento repozitář slouží také jako průvodce pro nové _inSIDEry_, kteří se chtějí podílet na vývoji některého z [našich projektů](https://gitlab.fel.cvut.cz/oi-side/projekty). 💻 Mrkni se do [prvního commitu](https://gitlab.fel.cvut.cz/oi-side/projekty/czech-crunch/-/commit/ddc6cbb5e96adc3feee1e7c4d072e3ee5a227587) a pročti si naše doporučení pro práci se _sajdovským_ GitLabem. 🎉
